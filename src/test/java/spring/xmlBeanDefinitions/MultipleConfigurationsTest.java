package spring.xmlBeanDefinitions;

import spring.xmlBeanDefinitions.testClasses.Person;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MultipleConfigurationsTest {
    @Test
    public void testMultipleConfigInXml() {
        ApplicationContext context = new ClassPathXmlApplicationContext("xmlBeanDefinitions/multipleConfigurations.xml");

        showInfoAboutPersonsFromContext(context);
    }

    @Test
    public void testMultipleXmlAppendedInJava() {
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {
                "xmlBeanDefinitions/subConfigsForCustomer/customerBean1.xml",
                "xmlBeanDefinitions/subConfigsForCustomer/customerBean2.xml"});

        showInfoAboutPersonsFromContext(context);

    }

    private void showInfoAboutPersonsFromContext(ApplicationContext context) {
        Person p1 = (Person)context.getBean("person1");
        System.out.println(p1);

        Person p2 = (Person)context.getBean("person2");
        System.out.println(p2);
    }
}
