package spring.xmlBeanDefinitions;

import spring.xmlBeanDefinitions.testClasses.Person;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class InjectValueIntoClassTest {
    @Test
    public void injectPropertiesDefinedInNormalWay() {
        ApplicationContext context = new ClassPathXmlApplicationContext("xmlBeanDefinitions/injectValueIntoBean.xml");

        Person person = (Person) context.getBean("personNormalProperties");
        System.out.println(person.toString());
    }

    @Test
    public void injectPropertiesDefinedUsingShortcuts() {
        ApplicationContext context = new ClassPathXmlApplicationContext("xmlBeanDefinitions/injectValueIntoBean.xml");

        Person person = (Person) context.getBean("personUsingShortcut");
        System.out.println(person.toString());
    }
}
