package spring.xmlBeanDefinitions;

import spring.xmlBeanDefinitions.testClasses.CustomerService;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class BeanScopeTest {
    @Test
    public void testBeanWithSingletonState() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[] {"xmlBeanDefinitions/statesTypeContext.xml"});

        CustomerService custA = (CustomerService)context.getBean("customerServiceSingleton");
        custA.setMessage("Message by custA");
        System.out.println("Message : " + custA.getMessage());

        //retrieve it again
        CustomerService custB = (CustomerService)context.getBean("customerServiceSingleton");
        System.out.println("Message : " + custB.getMessage());
    }

    @Test
    public void testBeanWithPrototypeState() {
        ApplicationContext context =
                new ClassPathXmlApplicationContext(new String[] {"xmlBeanDefinitions/statesTypeContext.xml"});

        CustomerService custA = (CustomerService)context.getBean("customerServicePrototype");
        custA.setMessage("Message by custA");
        System.out.println("Message : " + custA.getMessage());

        //retrieve it again
        CustomerService custB = (CustomerService)context.getBean("customerServicePrototype");
        System.out.println("Message : " + custB.getMessage());
    }
}
