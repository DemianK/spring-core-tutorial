package spring.annotationBasedBeanDefinitions;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    public Order order() {
        Order order = new Order();
        order.setAmount(22);
        return order;
    }

    @Bean
    public Person sender() {
        Person person = new Person();
        person.setAddress("Address");
        person.setAge(25);
        person.setName("UserName");
        return person;
    }

    @Bean
    public Person anotherPerson() {
        Person person = new Person();
        person.setAddress("Address");
        person.setAge(25);
        person.setName("Another UserName");
        return person;
    }
}
