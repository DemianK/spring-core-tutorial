package spring.xmlBeanDefinitions.propertyFile;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    @SuppressWarnings("resource")
    public static void main(String[] args) {

        ApplicationContext context = new ClassPathXmlApplicationContext("xmlBeanDefinitions/propertyFile/xmlBeanDefinitions.xml");

        HelloWorld hello = (HelloWorld) context.getBean("helloWorldBean");

        hello.sayHello();
        hello.sayGoodbye();
    }
}