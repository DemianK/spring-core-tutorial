package spring.postProcessor.beanPostProcessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import spring.postProcessor.annotation.Transactional;

import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

public class TransactionalBeanPostProcessor implements BeanPostProcessor {
    private Map<String, Class> transactionalBeans = new HashMap<>();

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class beanClass = bean.getClass();
        if (beanClass.isAnnotationPresent(Transactional.class)) {
            transactionalBeans.put(beanName, beanClass);
        }

        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Class beanClass = transactionalBeans.get(beanName);
        if (beanClass != null) {
            return Proxy.newProxyInstance(beanClass.getClassLoader(), beanClass.getInterfaces(), (proxy, method, args) -> {
                System.out.println("****TRANSACTIONAL****");
                Object retVal = method.invoke(bean, args);
                System.out.println("****TRANSACTIONAL****");
                return retVal;
            });
        }
        return bean;
    }
}
